const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const employee = require('../controllers/employee');


const schema = mongoose.Schema({
    _firstName: String,
    _lastName : String,
    _email : String,
    _phoneNumber : String,
    _hireDate : Date,
    _jobId : String,
    _salary : Number,
    _commisionPct : Number,
    _managerId : String,
    _departmentId : String
});

class employee{
    constructor(firstName, lastName, email, phoneNumber, hireDate, jobId, salary, commisionPct, managerId, departmentId){
        this._firstName = firstName;
        this._lastName = lastName;
        this._email = email;
        this._phoneNumber = phoneNumber;
        this._hireDate = hireDate;
        this._jobId = jobId;
        this._salary = salary;
        this._commisionPct = commisionPct;
        this.managerId = managerId;
        this._departmentId = departmentId; 
    }

    get employeeFirstName(){
        return this._firstName;
    }
    set employeeFirstName(v){
        this._firstName = v;
    }
    get employeeLastName(){
        return this._lastName;
    }
    set employeeLasttName(v){
        this._lastName = v;
    }
    get employeeEmail(){
        return this._email;
    }
    set employeeEmail(v){
        this._email = v;
    }
    get employeePhoneNumber(){
        return this._phoneNumber;
    }
    set employeePhoneNumber(v){
        this._phoneNumber = v;
    }
    get employeeHireDate(){
        return this._hireDate;
    }
    set employeeHireDate(v){
        this._hireDate = v;
    }
    get employeeJobId(){
        return this._jobId;
    }
    set employeeJobId(v){
        this._jobId = v;
    }
    get employeeSalary(){
        return this._salary;
    }
    set employeeSalary(v){
        this._salary = v;
    }
    get employeeCommisionPct(){
        return this._commisionPct;
    }
    set employeeCommisionPct(v){
        this._commisionPct = v;
    }
    get employeeManagerId(){
        return this._managerId;
    }
    set employeeManagerId(v){
        this._managerId = v;
    }
    get employeeDepartmentId(){
        return this._departmentId;
    }
    set employeeDepartmentId(v){
        this._departmentId = v;
    }
}


schema.plugin(mongoosePaginate);
schema.loadClass(employee);
module.exports = mongoose.model('employee', schema);

