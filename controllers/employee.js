const async = require('async');
const bcrypt = require('bcrypt');
const { json } = require('express');
const Employee = require('../models/employee');

function list(req,res, next){
    const page = req.query.page ? req.query.page : 1;
    const limit = req.query.page ? req.query.page : 10;

    Employee.paginate(req, 
        {page: page, limit: limit}).then(employee => res.status(200).json({
          message: res.__('employee.list.ok'),
          objs: employee
      })).catch(error => res.status(500).json({
          message: res.__('employee.list.err'),
          obj: error
      }));
}

function index(req,res){
    let id= req.params.id;
    Employee.findOne({_id: id}).then(employee => res.status(200).json({
        message: res._('employee.index.ok'),
        objs: employee
    })).catch(error => res.status(500).json({
        message: res._('employee.index.err'),
        obj: error
    }))
}
function create(req,res){
    let firstName = req.body.firstName;
    let lastName = req.body.lastName;
    let email = req.body.email;
    let phoneNumber = req.body.phoneNumber;
    let hireDate = req.body.hireDate;
    let jobId = req.body.jobId;
    let salary = req.body.salary;
    let commisionPct = req.body.commisionPct;
    let managerId = req.body.commisionPct;
    let departmentId = req.body.departmentId;

    let employee = new Employee({
        _firstName : firstName,
        _lastName : lastName,
        _email : email,
        _phoneNumber : phoneNumber,
        _hireDate : hireDate,
        _jobId : jobId,
        _salary : salary,
        _commisionPct : commisionPct,
        _managerId : managerId,
        _departmentId : departmentId 
    })

    employee.save().then(employee => res.status(200).json({
        message : res.__('employee.create.ok'),
        objs: employee
    })).catch(err => res.status(500).json({
        message : res.__('employee.create.err'),
        obj : err
    }));



}

function update(req,res){
    let id = req.params.id;

    let employee = new Object();

    if(req.body.firstName)
      employee._firstName = req.body.firstName;
    if(req.body.lastName)
      project._lastName = req.body.lastName;
    if(req.body.email)
      project._email = req.body.email;
    if(req.body.phoneNumber)
      project._phoneNumber = req.body.phoneNumber;
    if(req.body.hireDate)
      project._hireDate = req.body.hireDate;
    if(req.body.jobId)
      project._jobId = req.body.jobId;
    if(req.body.salary)
      project._salary = req.body.salary;
    if(req.body.commisionPct)
      project._commisionPct = req.body.commisionPct;
    if(req.body.managerId)
      project._managerId = req.body.managerId;
    if(req.body.departmentId)
      project._departmentId = req.body.departmentId;

    Employee.findOneAndUpdate({_id: id}, {omitUndefined: true}).then(employee => res.status(200).json({
        message : res.__('employee.update.ok'),
        objs: employee
    })).catch(error => res.status(500).json({
        message: res.__('employee.update.err'),
        obj: error
    }));
}

function destroy(req,res){
    const id = req.params.id;
    Employee.deleteOne({_id: id}).then(project => res.status(200).json({
        message: res.__('employee.destroy.ok'),
        objs: error
    })).catch(error => res.status(500).json({
        message: res.__('employee.destroy.err'),
        obj: error
    }));
}

module.exports = {
    list,
    create,
    update,
    destroy,
    index
}

